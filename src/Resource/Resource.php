<?php
namespace Mailtrigger\Api\Resource;

use Mailtrigger\Api\MailtriggerApi;

/**
 * Class Resource
 *
 * @package Tourpaq\Soap\Resource
 */
class Resource
{

    protected $MailtriggerApi;

    /**
     * Constructor to bind the $MailplatformApi object
     *
     * @param $MailplatformApi $MailplatformApi
     */
    public function __construct(MailtriggerApi $MailtriggerApi)
    {
        $this->MailtriggerApi = $MailtriggerApi;
    }
}
