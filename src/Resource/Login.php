<?php
namespace Mailtrigger\Api\Resource;

/**
 * Class Triggers
 *
 * @package Mailplatform\Api\Subscribers
 */
class Login extends Resource
{

    public function auth($email, $password)
    {
        $bodyParameters = [
            'email'    => $email,
            'password' => $password,
        ];

        return $this->MailtriggerApi->callAPI('login', 'POST', $bodyParameters);
    }
}