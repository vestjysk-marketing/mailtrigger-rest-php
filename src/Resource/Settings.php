<?php
namespace Mailtrigger\Api\Resource;

/**
 * Class Settings
 *
 * @package Mailplatform\Api\Subscribers
 */
class Settings extends Resource
{

    public function getMailtrigger()
    {
        return $this->MailtriggerApi->callAPI("settings/mailtrigger", 'GET');
    }

    public function putMailtrigger($bodyParameters = [])
    {
        return $this->MailtriggerApi->callAPI('settings/mailtrigger', 'PUT', $bodyParameters);
    }
}
