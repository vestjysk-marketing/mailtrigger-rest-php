<?php
namespace Mailtrigger\Api\Resource;

/**
 * Class Leads
 *
 * @package Mailplatform\Api\Subscribers
 */
class Leads extends Resource
{

    public function put($bodyParameters = [])
    {
        return $this->MailtriggerApi->callAPI('lead', 'PUT', $bodyParameters);
    }

    public function getAll($bodyParameters = [])
    {
        return $this->MailtriggerApi->callAPI('leads', 'GET', $bodyParameters);
    }

    public function get($leadId)
    {
        return $this->MailtriggerApi->callAPI('lead/' . $leadId, 'GET');
    }
}