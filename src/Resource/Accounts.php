<?php
namespace Mailtrigger\Api\Resource;

/**
 * Class Accounts
 *
 */
class Accounts extends Resource
{

    public function get()
    {
        return $this->MailtriggerApi->callAPI('account', 'GET');
    }

    public function put($bodyParameters = [])
    {
        return $this->MailtriggerApi->callAPI('account', 'PUT', $bodyParameters);
    }
}