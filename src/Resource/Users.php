<?php
namespace Mailtrigger\Api\Resource;

/**
 * Class Users
 *
 * @package Mailplatform\Api\Subscribers
 */
class Users extends Resource
{

    public function get()
    {
        return $this->MailtriggerApi->callAPI('users', 'GET');
    }

    public function getById($userId)
    {
        return $this->MailtriggerApi->callAPI('user/' . $userId, 'GET');
    }

    public function put($bodyParameters = [])
    {
        return $this->MailtriggerApi->callAPI('user', 'PUT', $bodyParameters);
    }
}