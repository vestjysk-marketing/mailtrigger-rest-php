<?php
namespace Mailtrigger\Api\Resource;

/**
 * Class Triggers
 *
 * @package Mailplatform\Api\Subscribers
 */
class Triggers extends Resource
{

    public function getAll($bodyParameters = [])
    {
        return $this->MailtriggerApi->callAPI('triggers', 'GET', $bodyParameters);
    }

    public function get($triggerId)
    {
        return $this->MailtriggerApi->callAPI('trigger/' . $triggerId, 'GET');
    }

    public function delete($triggerId)
    {
        return $this->MailtriggerApi->callAPI('trigger/' . $triggerId, 'DELETE');
    }

    public function put($bodyParameters = [])
    {
        return $this->MailtriggerApi->callAPI('trigger', 'PUT', $bodyParameters);
    }

    public function leadsForTrigger($triggerId, $bodyParameters = [])
    {
        return $this->MailtriggerApi->callAPI('trigger/leads/' . $triggerId, 'GET', $bodyParameters);
    }

    public function addSentLead($bodyParameters = [])
    {
        return $this->MailtriggerApi->callAPI('trigger/lead/addsent', 'PUT', $bodyParameters);
    }
    public function addErrorLead($bodyParameters = [])
    {
        return $this->MailtriggerApi->callAPI('trigger/lead/adderror', 'PUT', $bodyParameters);
    }
    public function addUrls($bodyParameters = [])
    {
        return $this->MailtriggerApi->callAPI('trigger/addurls', 'POST', $bodyParameters);
    }

}