<?php
namespace Mailtrigger\Api\Resource;

/**
 * Class Visits
 *
 * @package Mailtrigger\Api\Resource
 */
class Visit extends Resource
{
    public function visits($bodyParameters = [])
    {
        return $this->MailtriggerApi->callAPI('visits', 'GET', $bodyParameters);
    }
    
    public function getUrlsFromLead($leadId, $bodyParameters = [])
    {
        return $this->MailtriggerApi->callAPI('visits/'.$leadId, 'GET', $bodyParameters);
    }
}