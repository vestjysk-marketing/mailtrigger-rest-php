<?php
namespace Mailtrigger\Api;

use Mailtrigger\Api\Resource;

/**
 * Class MailplatformApi
 *
 * @version 1.0.0
 * @package MailplatformApi
 */
class MailtriggerApi
{

    /**
     * methods used
     */
    private $triggers, $leads, $users, $settings;

    /**
     * @var string $apiKey
     */
    public $apiKey;

    /**
     * @var bool $returnAsObject
     */
    public $returnAsArray = true;

    /**
     * @var string $apiPath
     */
    public $baseUrl = 'https://www.mail-trigger-api.com/api/';

    /**
     * MailplatformApi constructor.
     *
     * @param $apiKey
     */
    public function __construct($apiKey)
    {
        /**
         * Set $apiKey
         */
        $this->apiKey = $apiKey;

        /**
         * Init methods
         */
        $this->triggers = new Resource\Triggers($this);
        $this->leads    = new Resource\Leads($this);
        $this->users    = new Resource\Users($this);
        $this->accounts = new Resource\Accounts($this);
        $this->settings = new Resource\Settings($this);
        $this->visit    = new Resource\Visit($this);
    }

    /**
     * @return Resource\Triggers
     */
    public function triggers()
    {
        return $this->triggers;
    }

    /**
     * @return Resource\Leads
     */
    public function leads()
    {
        return $this->leads;
    }

    /**
     * @return Resource\Users
     */
    public function users()
    {
        return $this->users;
    }

    /**
     * @return Resource\Accounts
     */
    public function accounts()
    {
        return $this->accounts;
    }

    /**
     * @return Resource\Settings
     */
    public function settings()
    {
        return $this->settings;
    }

    public function visit()
    {
        return $this->visit;
    }

    /**
     * @param        $uri
     * @param string $method
     * @param array  $bodyParameters
     * @param array  $options
     *
     * @return mixed
     */
    public function callAPI($uri, $method = 'GET', $bodyParameters = [], $options = [])
    {

        //replace baseurl in API URI if present
        $uri = str_replace($this->baseUrl, '', $uri);

        /**
         * Setup CURL
         */
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->baseUrl . $uri);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERPWD, ":" . $this->apiKey);

        // Use body parameters for all requests except GET
        if ($method == 'POST' || $method == 'DELETE') {
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($bodyParameters));
        }

        // Determines the method to use
        if ($method == 'POST') {
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($bodyParameters));
        } else {
            if ($method == 'GET') {
                $full_uri_get = $this->baseUrl . $uri;

                if ($bodyParameters) {
                    $full_uri_get = $full_uri_get . '?' . http_build_query($bodyParameters);
                }
                curl_setopt($ch, CURLOPT_HTTPGET, true);
                curl_setopt($ch, CURLOPT_URL, $full_uri_get);
            } else {
                if ($method == 'PUT') {
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($bodyParameters));
                } else {
                    if ($method == 'DELETE') {
                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
                    }
                }
            }
        }

        // Performs request
        $resultBody = curl_exec($ch);

        //get status code
        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code

        //parse as array or object
        $result = json_decode($resultBody, $this->returnAsArray);

        //close connection
        curl_close($ch);

        //handle data
        if ($statusCode == 401) {
        } else {
            if ($statusCode == 200) { // Success
                return $result;
            } else { // Failed
                return $result;
            }
        }
    }
}
